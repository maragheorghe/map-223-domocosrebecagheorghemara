package com.example.map223rebecadomocosmaragheorghe.utils.constants;

import java.time.format.DateTimeFormatter;

public class Constants {
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm");
    public static final String url = "jdbc:postgresql://localhost:5432/MAP";
    public static final String username = "postgres";

//    public static final String password = "parola";
//
//    public static final String groupPhoto = "file:/C:/Git/pictures/chatPictures/profilGroup.png";
//    public static final String photoUrlProfile = "file:/C:/Git/pictures/profilePictures/profilDefault.jpeg";
//    public static final String photoUrlEvent = "file:/C:/Git/pictures/eventPictures/profilEvent.jpeg";

    public static final String password = "3cd3eJ29b2";
    public static final String groupPhoto = "file:\\C:\\Git\\groupPictures\\profilGroup.png";
    public static final String photoUrlProfile = "file:\\C:\\Git\\profilePictures\\profilDefault.jpeg";
    public static final String photoUrlEvent = "file:\\C:\\Git\\eventPictures\\profilEvent.jpeg";

}
